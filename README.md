# Node, Git and PowerShell Installation Windows

## Node Installation
To install NodeJS, I recommended we use nvm or Node Version Manager. The reason why we need it, it is because it makes installation easier and makes switching of node version quicker.

Navigate to this repository in github and download the `nvm-setup.zip`.

[https://github.com/coreybutler/nvm-windows/releases/tag/1.1.7](https://github.com/coreybutler/nvm-windows/releases/tag/1.1.7)

To check if you properly installed nvm, open a terminal and type `nvm --version` or `nvm -v`. If you see a version of nvm shows up, then you have it installed.

To install node with the latest version, the command is `nvm install latest` and for a specific version please do `nvm install <version number>`. For example, `nvm install 12.38.1`.

To use the version of node that you just installed, you can do `nvm use <version number>`.

To check what version of node you are using. The command for that is `node --version`.

To check the current list of node you have in your system. The command for that is `mvn list`.

## Git Installation
To install Git you need to go to the git websize and download/install the exe file.
[https://git-scm.com/downloads](https://git-scm.com/downloads)

After you installed git in your system, now you need to decided to choose between git bash or PowerShell as your default terminal.

If you decided to use git bash as your terminal, then you do not need to do any configuration. You just need to search the terminal in your computer.

Now if you decided to use PowerShell, then you need to do a little configuration. Also, PowerShell always comes with a windows device.

The first thing you need to do, is to find and open `Windows PowerShell` and let's setup our global git username and email.

```
git config --global user.name <user.name>
git config --global user.email <email>
```
## PowerShell Installation
Once you had set up your git user name and email, the next step is to do install `Posh-Git`. Open a new Windows PowerShell as Administrator and run this command `Install-Module Posh-Git`. Then if you are ask some question, choose and `yes` and `yes to all`.

Next navigate to `This PC > Documents > WindowsPowerShell` then make a new file called profile.ps1. And inside the file type `Import-Module Posh-Git` and save the file. So what it does is, every time you open PowerShell it will be able to use git.

Now for the last step, please close all your PowerShell Terminal. And then open a new PowerShell and enter this command `%USERPROFILE%/Documents/WindowsPowerShell` .

I hope this helps get you started developing or automating your application. Cheers!
